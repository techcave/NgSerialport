串口助手是嵌入式开发中常用到的一个桌面工具，用于串口调试，而Angular 是一个Web应用框架。桌面端原生功能和硬件操作似乎永远都不会和Web发生直接关系。然而，随着JavaScript技术的进步和变革，一切都成为可能。Electron 是一个使用 JavaScript, HTML 和 CSS 等 Web 技术创建原生程序的框架，结合Electron可以构建兼容 Mac, Windows 和 Linux的应用程序。同时也可以使用文件、串口的系统原生功能，这里我们开发一个串口助手来说明如何融合这些技术。

通过这个分享可以get到Angular开发技能、Electron桌面打包技能和如何调用原生功能的技能，同时还获得一个串口工具，主要包含以下内容：

1. 开发环境和工具的搭建
2. 在Angular中如何配置Electron
3. Electron中使用Serialport库的配置
4. 在Angular中如何利用Electrong调用原生功能
5. Angular、Electron、node-serialport基础开发知识
6. NSIS安装程序制作

[点击进入主题:使用网页技术开发串口助手
](https://gitbook.cn/gitchat/activity/5c1067844ab67327814c5248
)

![TIM图片20181212160036.jpg](https://upload-images.jianshu.io/upload_images/3332945-ae286ac32dec95cd.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
# NgSerialport

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
