import { Component, OnInit, NgZone } from '@angular/core';
import * as SerialPort from 'serialport'
import { ElectronService } from 'ngx-electron';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  /**
 * @typedef {Object} openOptions
 * @property {boolean} [autoOpen=true] Automatically opens the port on `nextTick`.
 * @property {number=} [baudRate=9600] The baud rate of the port to be opened. This should match one of the commonly available baud rates, such as 110, 300, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 57600, or 115200. Custom rates are supported best effort per platform. The device connected to the serial port is not guaranteed to support the requested baud rate, even if the port itself supports that baud rate.
 * @property {number} [dataBits=8] Must be one of these: 8, 7, 6, or 5.
 * @property {number} [highWaterMark=65536] The size of the read and write buffers defaults to 64k.
 * @property {boolean} [lock=true] Prevent other processes from opening the port. Windows does not currently support `false`.
 * @property {number} [stopBits=1] Must be one of these: 1 or 2.
 * @property {string} [parity=none] Must be one of these: 'none', 'even', 'mark', 'odd', 'space'.
 * @property {boolean} [rtscts=false] flow control setting
 * @property {boolean} [xon=false] flow control setting
 * @property {boolean} [xoff=false] flow control setting
 * @property {boolean} [xany=false] flow control setting
 * @property {object=} bindingOptions sets binding-specific options
 * @property {Binding=} Binding The hardware access binding. `Bindings` are how Node-Serialport talks to the underlying system. Will default to the static property `Serialport.Binding`.
 * @property {number} [bindingOptions.vmin=1] see [`man termios`](http://linux.die.net/man/3/termios) LinuxBinding and DarwinBinding
 * @property {number} [bindingOptions.vtime=0] see [`man termios`](http://linux.die.net/man/3/termios) LinuxBinding and DarwinBinding
 */
  openOption = {
    port: '',
    autoOpen: true,
    baudRate: 9600,
    dataBits: 8,
    highWaterMark: 65536,
    lock: true,
    stopBits: 1,
    parity: "none",
    rtscts: false,
    xon: false,
    xoff: false,
    xany: false
  }
  serialport: typeof SerialPort = null
  portsInfo = []
  port = null;
  rcvData = null;
  sendData = null;
  constructor(private electronSvc: ElectronService, private zone: NgZone) {
    if (this.electronSvc.isElectronApp) {
      this.serialport = this.electronSvc.remote.getGlobal("serialport")
      this.serialport.list().then(val => {
        console.log("ports is ", val)
        this.portsInfo = val
      }, reason => {
        console.log("reason is ", reason)
      })
    }
  }
  compareFn(c1, c2): boolean {
    // console.log("c1 c2 is ", c1, c2, c1 && c2 ? c1 == c2 : false)
    return c1 && c2 ? c1 == c2 : false;
  }
  ngOnInit() {
    // this.serialport a
    if (this.electronSvc.isElectronApp) {
      this.serialport.list().then(val => {
        console.log("ports is ", val)
        this.portsInfo = val
      }, reason => {
        console.log("reason is ", reason)
      })
    }
  }

  doConnect() {
    // 新建端口实例，因为autoOpen为true，所以不用显式open，否则手动调用open方法打开端口
    this.port = new this.serialport(this.openOption.port, this.openOption, err => {
      console.log("err is ", err)
    })
    // 端口监听接收数据
    this.port.on("data", (data) => {
      this.zone.run(() => {
        this.rcvData = data
      })
      
    })
  }

  send() {
    if (this.sendData) {
      this.port.write(this.sendData)
    }
  }

}
