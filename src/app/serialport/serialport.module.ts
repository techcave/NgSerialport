import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxElectronModule} from 'ngx-electron';
import { SerialportRoutingModule } from './serialport-routing.module';
import { MainComponent } from './page/main/main.component';
import { FormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { HexEditorComponent } from './page/hex-editor/hex-editor.component';

@NgModule({
  declarations: [MainComponent, HexEditorComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgZorroAntdModule,
    NgxElectronModule,
    SerialportRoutingModule
  ]
})
export class SerialportModule { }
