/**
 * electron 入口文件
 */
// 引入其他库
const { app, BrowserWindow, Menu, ipcMain, dialog, shell, globalShortcut } = require('electron')
const path = require('path')
const url = require('url')
const fs = require('fs')
const serialport = require("serialport")

/**
 * 程序就绪的时候，主要是创建窗口和初始化
 */
app.on('ready', createWindow)

/**
 * 程序关闭时执行的操作
 */
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})



/**
 * 创建窗口
 */
function createWindow() {

  // 新建浏览器窗口，就是壳
  console.log("start create windows")
  win = new BrowserWindow({
    width: 800, height: 600, webPreferences: {
      //避免错误：Not allowed to load local resource
      webSecurity: false
    }
  })

  // 全局变量，这里把原生库通过全局变量分享出来
  global.serialport = serialport

  // 自定义菜单
  // const menu = Menu.buildFromTemplate(template)
  // Menu.setApplicationMenu(menu)
  // 隐藏菜单（我们不需要菜单）
  Menu.setApplicationMenu(null)

  // win = new BrowserWindow()
  // 打开开发人员工具，用于调试阶段
  win.webContents.openDevTools()
  try {
    debugger;
    console.log(path.resolve('dist/NgSerialport/index.html'))
    console.log(path.join(__dirname, '../dist/NgSerialport/index.html'))

    // load the dist folder from Angular(入口文件)
    // setTimeout(() => {
    win.loadURL(url.format({
      pathname: path.join(__dirname, '../dist/NgSerialport/index.html'),
      protocol: 'file:',
      slashes: true
    }))
    // 从编译结果路径加载页面
    // win.loadURL(url.format({
    //   pathname: path.resolve('./dist/NgSerialport/index.html'),
    //   protocol: 'file:',
    //   slashes: true
    // }))
    // }, 2000)
  } catch (ex) {
    console.log("error")
    console.log("ex is ", ex)
  }
  console.log("app loaded");

  // 处理窗口关闭事件
  win.on('closed', () => {
    win = null
  })
}

/**
 * 这里演示了主进程如何接收消息
 */
ipcMain.on('custom-event-name', function (event) {
  console.log("here is event-process")
})